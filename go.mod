module bitbucket.org/oeufdure/freeradius-go-modules

go 1.17

require (
	github.com/davecgh/go-spew v1.1.1
)

require bitbucket.org/oeufdure/freeradius-go v0.0.0-20220412011004-1100da2aaac8 // indirect
